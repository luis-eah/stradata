<?php

namespace Database\Seeders;

use App\Models\Search;
use Illuminate\Database\Seeder;

class SearchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Search::create([
            "name" => "gestion_humana.certificados.solicitar",
            "nombre" => "Solicitar certificados",
            "descripcion" => "Otorga el permiso para solicitar los certificados."
        ]);
    }
}
