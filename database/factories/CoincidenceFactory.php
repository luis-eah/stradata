<?php

namespace Database\Factories;

use App\Models\Coincidence;
use App\Models\Search;
use Illuminate\Database\Eloquent\Factories\Factory;

class CoincidenceFactory extends Factory
{
    protected $model = Coincidence::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'query' => $this->faker->name(),
            'match_per' => $this->faker->numberBetween(1,100),
            'status' => $this->faker->randomElement([Search::RESULT,Search::NO_RESULT,Search::ERROR]),
        ];
    }
}
