<?php

namespace Database\Factories;

use App\Models\Search;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class SearchFactory extends Factory
{
    protected $model = Search::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'query' => $this->faker->name(),
            'match_per' => $this->faker->numberBetween(1,100),
            'status' => $this->faker->randomElement([Search::RESULT,Search::NO_RESULT,Search::ERROR]),
        ];
    }
}
