<?php

use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\CoincidenceController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\SearchController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('customer', [CustomerController::class, "index"])
    // ->middleware('auth:sanctum')
    ->name("customer");



Route::apiResource("search", SearchController::class);

Route::get('search/{search:uuid}', [SearchController::class, "show"])
    // ->middleware('auth:sanctum')
    ->name("search.get")
    ;


Route::post('login', [LoginController::class, "login"])
    ->name("login");


Route::apiResource("coincidence", CoincidenceController::class)
    ->middleware('auth:sanctum');

