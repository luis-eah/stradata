<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coincidence extends Model
{
    use HasFactory;  

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'customer_id',
        'search_id',
        'match_per',
    ];

    /**
     * Función para dar la busqueda asociada a la coincidencias.
     */
    public function search()
    {
        return $this->belongsTo(Search::class);
    }

    /**
     * Función para dar la busqueda asociada a la coincidencias.
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }


}
