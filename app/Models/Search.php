<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    use HasFactory;


    const RESULT = 1;
    const NO_RESULT = 0;
    const ERROR = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'query',
        'query_clean',
        'match_per',
        'status',
        'uuid',
    ];

    /**
     * Get the phone record associated with the user.
     */
    public function coincidences()
    {
        return $this->hasMany(Coincidence::class, 'search_id');
    }

}
