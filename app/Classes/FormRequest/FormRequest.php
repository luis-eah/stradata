<?php

namespace App\Classes\FormRequest;

use Illuminate\Foundation\Http\FormRequest as FormRequestOriginal;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Str;

abstract class FormRequest extends FormRequestOriginal
{

    protected $redirect = "/validaciones";
    protected $redirectRoute = 'validaciones';
    protected $fechas = [];
    protected $mayusculas = [];
    protected $minusculas = [];
    protected $titulos = [];
    protected $jsons = "";
    protected $formatoFechas = "d/m/Y";

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function passedValidation()
    {
        if (!empty($this->fechas)) {
            $this->fechas($this->formatoFechas);
        }
        if (!empty($this->mayusculas)) {
            $this->mayusculas();
        }
        if (!empty($this->minusculas)) {
            $this->minusculas();
        }
        if (!empty($this->titulos)) {
            $this->titulos();
        }
        if (!empty($this->jsons)) {
            $this->jsons();
        }
    }



    /**
     * Función que convierte todos los strings que hayan sido seleccionados
     * a mayúsculas.
     */
    public function mayusculas()
    {
        $campos = $this->mayusculas ?? [];
        $info = $this->only($campos);
        $upper = function ($texto) {
            if (isset($texto)) {
                return Str::upper($texto);
            }
        };
        $this->merge(array_map($upper, $info));
    }



    /**
     * Función que convierte todos los strings que hayan sido seleccionados
     * a minúsculas.
     */
    public function minusculas()
    {
        $campos = $this->minusculas ?? [];
        $info = $this->only($campos);
        $upper = function ($texto) {
            if (isset($texto)) {
                return Str::lower($texto);
            }
        };
        $this->merge(array_map($upper, $info));
    }

    /**
     * Función que convierte todos los strings que hayan sido seleccionados a
     * un formato como si fuese un título.
     */
    public function titulos()
    {
        $campos = $this->titulos ?? [];
        $info = $this->only($campos);
        $upper = function ($texto) {
            if (isset($texto)) {
                return Str::title($texto);
            }
        };
        $this->merge(array_map($upper, $info));
    }

    /**
     * Función que transforma los strings de fechas a un objeto
     * Carbon.
     * @param string $formato Formato de la fecha. Por defecto d/m/Y.
     */
    public function fechas($formato = "d/m/Y")
    {
        $campos = $this->fechas ?? [];
        $info = $this->only($campos);
        $upper = function ($texto) use ($formato) {
            if (isset($texto)) {
                return Carbon::createFromFormat($formato, $texto);
            }
        };
        $this->merge(array_map($upper, $info));
    }

    /**
     * Función que permite transformar un campo string a JSON.
     * @param bool $assoc Traer el JSON como un arreglo asociativo o un objeto. Por defecto falso.
     */
    public function jsons($assoc = false)
    {
        $campos = $this->jsons ?? "";
        $info = $this->only($campos);
        $upper = function ($texto) use ($assoc) {
            if (isset($texto) && is_string($texto)) {
                return json_decode($texto, $assoc);
            }
        };
        $this->merge(array_map($upper, $info));
    }

    /**
     *
     */
    protected function failedValidation(Validator $validator)
    {
        $errors["estado"] = "error";
        $errors['mensaje']       = 'Existen errores de validación';
        $errors['validaciones']  = $validator->errors()->all();
        $errors['data']          = $validator->getData();
        $errors["statusCode"] = 422;
        throw new HttpResponseException(response()->json($errors, 422));
    }
}
