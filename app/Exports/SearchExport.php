<?php

namespace App\Exports;

use App\Models\Search;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class SearchExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        // resources\views\search\exports\principal.blade.php
        // return Search::all();
        return view("search\exports\principal");
    }
}
