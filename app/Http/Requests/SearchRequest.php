<?php

namespace App\Http\Requests;

// use App\Classes\FormRequest\FormRequest;

// use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

use App\Classes\FormRequest\FormRequest;

class SearchRequest extends FormRequest
{

    // protected $minusculas = ["query"];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'query'          => 'required|string',
            'match_per'          => 'required|regex:/^\d+(\.\d{1,2})?$/|between:1,100',
        ];
    }

}
