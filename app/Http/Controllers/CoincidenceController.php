<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCoincidenceRequest;
use App\Http\Requests\UpdateCoincidenceRequest;
use App\Models\Coincidence;

class CoincidenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("welcome");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCoincidenceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCoincidenceRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coincidence  $coincidence
     * @return \Illuminate\Http\Response
     */
    public function show(Coincidence $coincidence)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCoincidenceRequest  $request
     * @param  \App\Models\Coincidence  $coincidence
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCoincidenceRequest $request, Coincidence $coincidence)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coincidence  $coincidence
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coincidence $coincidence)
    {
        //
    }
}
