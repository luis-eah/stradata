<?php

namespace App\Http\Controllers;

use App\Exports\SearchExport;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\StoreSearchRequest;
use App\Http\Requests\UpdateSearchRequest;
use App\Http\Resources\SearchResource;
use App\Models\Customer;
use App\Models\Search;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request)
    {
        $search = $request->input("query");
        $percentage = $request->input("match_per");
        // $customers = Customer::limit(5)->get();

        $search = Search::create([
            'query' =>$search ,
            'query_clean' =>clean($search),
            'match_per' =>$percentage,
            'uuid' => uuid(),
        ]);
        // $search->load("coincidences");
        dd($search->toArray());
        return $search;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSearchRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSearchRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Search  $search
     * @return \Illuminate\Http\Response
     */
    public function show(Search $search)
    {
        $search->load("coincidences");
        return new SearchResource($search);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Search  $search
     * @return \Illuminate\Http\Response
     */
    public function edit(Search $search)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSearchRequest  $request
     * @param  \App\Models\Search  $search
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSearchRequest $request, Search $search)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Search  $search
     * @return \Illuminate\Http\Response
     */
    public function destroy(Search $search)
    {
        //
    }


    public function export(Search $search)
    {
        $fecha = now()->format('Y-m-d h:i:s a');
        return Excel::download(new SearchExport($search), "Resultado Fecha $fecha.xlsx");
    }
}
