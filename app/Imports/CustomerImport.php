<?php

namespace App\Imports;

use App\Models\Customer;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;


class CustomerImport implements ToModel, WithStartRow, WithCustomCsvSettings
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Customer([
            'state'         => $row[0],
            'locality'      => $row[1],
            'municipality'  => $row[2],
            'full_name'     => $row[3],
            'active_years'  => $row[4],
            'position'      => $row[5],
            'person_type'   => $row[6],
        ]);
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ";"
        ];
    }

    /**
     * Funcion para definir la linea de importación del archivo csv
     */
    public function startRow(): int
    {
        return 2;
    }
}
