<?php

use Illuminate\Support\Str;

if (!function_exists('clean')) {
    function clean($text)
    {
        // Remove blanks
        $text = str_replace(' ', '', $text);
        // Lower Case Text
        $text =  Str::lower($text);
        return $text;
    }
}


if (!function_exists('phoneticComparison')) {
    function phoneticComparison($a,$b)
    {
        // Remove blanks
        return soundex($a) == soundex($b);
    }
}

if (!function_exists('uuid')) {
    function uuid()
    {
        // Remove blanks
       return (string) Str::uuid()    ;
    }
}