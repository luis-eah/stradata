<?php

namespace App\Observers;

use App\Models\Customer;
use App\Models\Search;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SearchObserver implements ShouldQueue
{

    /**
     * Handle the usuario "created" event.
     *
     * @param  \App\Models\Sistema\Search $search
     * @return void
     */
    public function created(Search $search)
    {
        $this->createCoincidence($search);
    }

    /**
     * Handle the usuario "updated" event.
     *
     * @param  \App\Models\Sistema\Search $search
     * @return void
     */
    public function updated(Search $search)
    {
    }

    /**
     * Handle the usuario "deleted" event.
     *
     * @param  \App\Models\Sistema\Search $search
     * @return void
     */
    public function deleted(Search $search)
    {
    }

    /**
     * Función para crear las coincidencias cuando se ha creado una busqueda.
     */
    public function createCoincidence($search)
    {
        $customers = Customer::limit(5)->get();
        // $customers = Customer::all();
        $exists = false;
        $result = null;

        try {

            foreach ($customers as $customer) {
                $auxPercentage = 0;
                $fullNameClean = clean($customer->full_name);

                similar_text($search->query_clean, $fullNameClean, $auxPercentage);

                if (
                    $auxPercentage >= $search->match_per
                    || phoneticComparison($search->query, $customer->full_name)
                    || clean($search) == clean($customer->full_name)
                ) {
                    $exists = true;

                    $info =  [
                        'customer_id' => $customer->id,
                        'search_id' => $search->id,
                        'match_per' => $auxPercentage,
                        'uuid' => uuid(),
                    ];
                    $search->coincidences()->create( $info );
                }
            }

            $result = $exists ? Search::RESULT : Search::NO_RESULT;

        } catch (\Throwable $th) {
            $result = Search::ERROR;
            Log::info(json_encode($th->getMessage()));

        }
        $search->status = $result;
        $search->save();
    }
}
